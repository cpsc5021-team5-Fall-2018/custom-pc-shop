function load-csv-template
{
local TABLENAME=$1
cat <<LOADCSV
-- $TABLENAME
LOAD DATA INFILE '/data/${TABLENAME}.csv' INTO TABLE \`$TABLENAME\`
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;
LOADCSV
}

TABLES=(
    VENDOR          PART_TYPE_LOOKUP    PART
    SOCKET          CPU                 FORM_FACTOR
    MOTHERBOARD     RAM                 STORAGE_TYPE
    STORAGE         CASE                PSU
    GPU             LOCATION            INVENTORY
    ZIP             CONTACT             PAYMENT_METHOD
    CUSTOMER        CONFIG              CONFIG_PART
    ORDER_STATUS    ORDER               ORDER_CONFIG
    COOLER)

for TABLE in ${TABLES[@]}; do
    load-csv-template $TABLE
done | mysql -u root -pcustompc CUSTOMPC
