# Introduction
This is a database for a build-to-order operation that fulfills custom gaming PC orders. It manages inventory as customers specify custom PC builds and submit orders, and as these orders are fulfilled and shipped. The data model is defined in [data-model.sql](./data-model.sql) and the database is packaged and built in a MySQL database image.

## To update the data model
Edit the [data-model.sql](./data-model.sql) file, commit and push. An automated job will update the Docker image and send it to the Gitlab registry registry.gitlab.com/cpsc5021-team5-fall-2018/custom-pc-shop

## To use the database
### Prerequisite: Docker (https://docs.docker.com/install/)
> *NOTE:* If you have a MySQL instance running on you local machine, you will need to stop the mysqld daemon, as the container instance below will compete with you standalone instance for port `3306`.
> Otherwise, you can work around this by specifying `-p <other-port>:3306` (as opposed to `-p 3306:3306`)to forward the container's port 3306 to a custom port on the host machine.

```bash
# Login to the gitlab docker registry with this read-only deploy token (expires Dec 19, 2018)
docker login registry.gitlab.com -u gitlab+deploy-token-30915 -p LCv--WG6garFMyPoZHx_

# Get the latest Docker image
docker pull registry.gitlab.com/cpsc5021-team5-fall-2018/custom-pc-shop

# To run and auto-cleanup after stopping (see `stop` command below)
docker container run -d -p 3306:3306 --rm --name custom-pc-shop registry.gitlab.com/cpsc5021-team5-fall-2018/custom-pc-shop
```

### OR _don't_ auto-cleanup database after stopping (a Docker container instance will remain on your workstation until you explicitly remove it)
```bash
docker container run -d -p 3306:3306 --name custom-pc-shop registry.gitlab.com/cpsc5021-team5-fall-2018/custom-pc-shop
```


### Default Admin Credentials
* username: `root`
* password: `custompc`


### To stop database instance
```bash
docker container stop custom-pc-shop
```

## Setting up a MySQL database container as a sandbox for design experiments
```bash
docker container run -d -p 3306:3306 --rm --name sandbox -e MYSQL_ROOT_PASSWORD=custompc -e MYSQL_DATABASE=CUSTOMPC mysql:latest
```
