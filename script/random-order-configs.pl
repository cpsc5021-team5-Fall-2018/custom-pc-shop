use v5.26;
use Text::CSV;

my @config_ids = 1..23;

for my $order_id (1..7) {
    my $n_assoc = 1 + int rand 5;
    for (1..$n_assoc) {
        my $config_id = $config_ids[int rand $#config_ids];
        my $qty = 1 + int rand 5;
        say join ',', $order_id, $config_id, $qty;
    }
}
