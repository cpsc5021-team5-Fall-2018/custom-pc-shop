use v5.26;
use Text::CSV;

my @config_ids = 1..23;
my (@cas, @col, @cpu, @gpu, @mob, @psu, @ram, @sto);

open my $part, '<data/PART.csv';
my $csv = Text::CSV->new({ binary => 1 });

my @hdr = $csv->header($part);

while (my $row = $csv->getline($part)) {
    my ($part_id, $part_type) = @$row[0, -1];
    my $pid_arr = do {
        if ($part_type eq 'CAS') { \@cas }
        elsif ($part_type eq 'COL') { \@col }
        elsif ($part_type eq 'CPU') { \@cpu }
        elsif ($part_type eq 'GPU') { \@gpu }
        elsif ($part_type eq 'MOB') { \@mob }
        elsif ($part_type eq 'PSU') { \@psu }
        elsif ($part_type eq 'RAM') { \@ram }
        else { \@sto }
    };

    push @$pid_arr, $part_id;
}

for my $config_id (@config_ids) {
    my $case = $cas[int(rand($#cas))];
    my $cooling = $col[int(rand($#col))];
    my $cpu = $cpu[int(rand($#cpu))];
    my $gpu = $gpu[int(rand($#gpu))];
    my $motherboard = $mob[int(rand($#mob))];
    my $psu = $psu[int(rand($#psu))];
    my $ram = $ram[int(rand($#ram))];

    say join ',', $config_id, $case, 1;
    say join ',', $config_id, $cooling, 1;
    say join ',', $config_id, $cpu, 1;
    say join ',', $config_id, $gpu, 1;
    say join ',', $config_id, $motherboard, 1;
    say join ',', $config_id, $psu, 1;
    say join ',', $config_id, $ram, 2;
}
