FROM mysql:8.0.12
WORKDIR /data/
ENV MYSQL_ROOT_PASSWORD=custompc \
	MYSQL_DATABASE=CUSTOMPC
COPY dbinit/* /docker-entrypoint-initdb.d/
COPY data/*.csv /data/
COPY my.cnf /etc/mysql/my.cnf
RUN chmod 0644 /etc/mysql/my.cnf \
    && chown root:root /etc/mysql/my.cnf
EXPOSE 3306
