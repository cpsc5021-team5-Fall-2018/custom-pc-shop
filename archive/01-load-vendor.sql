-- VENDOR
LOAD DATA LOCAL INFILE './Data_Samples/VENDOR.csv' INTO TABLE VENDOR
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

-- PART_TYPE_LOOKUP
LOAD DATA LOCAL INFILE './Data_Samples/PART_TYPE_LOOKUP.csv' INTO TABLE PART_TYPE_LOOKUP
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

-- PART
LOAD DATA LOCAL INFILE './Data_Samples/PART.csv' INTO TABLE PART
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

-- SOCKET
LOAD DATA LOCAL INFILE './Data_Samples/SOCKET.csv' INTO TABLE SOCKET
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

