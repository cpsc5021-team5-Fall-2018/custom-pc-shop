-- MySQL dump 10.13  Distrib 8.0.13, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: CUSTOMPC
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CASE`
--

DROP TABLE IF EXISTS `CASE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `CASE` (
  `PART_ID` int(11) NOT NULL,
  `CASE_MAX_RAD_SUPPORT_MM` int(11) NOT NULL,
  `FORM_FACTOR_ID` int(11) NOT NULL,
  PRIMARY KEY (`PART_ID`),
  KEY `fk_CASE_PART1_idx` (`PART_ID`),
  KEY `fk_CASE_FORM_FACTOR1_idx` (`FORM_FACTOR_ID`),
  CONSTRAINT `fk_CASE_FORM_FACTOR1` FOREIGN KEY (`FORM_FACTOR_ID`) REFERENCES `FORM_FACTOR` (`form_factor_id`),
  CONSTRAINT `fk_CASE_PART1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CASE`
--

LOCK TABLES `CASE` WRITE;
/*!40000 ALTER TABLE `CASE` DISABLE KEYS */;
INSERT INTO `CASE` VALUES (1,120,1),(2,280,2),(3,280,2),(4,280,2);
/*!40000 ALTER TABLE `CASE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CONFIG`
--

DROP TABLE IF EXISTS `CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `CONFIG` (
  `CONFIG_ID` int(11) NOT NULL,
  `CONFIG_HASH` varchar(32) NOT NULL,
  PRIMARY KEY (`CONFIG_ID`),
  UNIQUE KEY `idConfiguration_UNIQUE` (`CONFIG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CONFIG`
--

LOCK TABLES `CONFIG` WRITE;
/*!40000 ALTER TABLE `CONFIG` DISABLE KEYS */;
INSERT INTO `CONFIG` VALUES (1,'9789cf683dd01316c67018849d10c549'),(2,'beab76a780c9e74b3f8d31d9097fa14f'),(3,'cd8500da9e8b0739cd6a900d7cc36580'),(4,'a6ff353b07c2fe5b81322c0ed12a1ed2'),(5,'b8dc5b275aa33ac26091e2a939c0528b'),(6,'aae9e5b7c1a27b2b57a6ba6276536e20'),(7,'eb676a4df10bffb3a9a72bb1fa6236ce'),(8,'904a2be523bafff4c465d9dd3808b9a6'),(9,'2097e7c7547df8e5c76333ac411ec6c4'),(10,'f6892dff54cafc42b13433f62087f124'),(11,'060df652a4e6d0f6d26b1abcd1beaca5'),(12,'eeb6ddd2376cd015b11e292844636ec9'),(13,'587238696044d7a169ea13eb3fb04c48'),(14,'9982663796a67c2c8284bff1efb2fbd6'),(15,'120385c0aebc47c2953fcb391973ae2e'),(16,'2914f342aa6ccb5cf287652e4f0dbea6'),(17,'639fcacee8344803e6e2196fefae9b32'),(18,'14ef9ad6e5c02d6ef5e062478510717d'),(19,'eb9880215bdb88178de823f1e8fcbd3b'),(20,'197adeb839d4aeb596b04fbf1751a423'),(21,'f183e91a004e8215eb9a3ab856d3526c'),(22,'66903fde8119ca66a96369caa7f5cedf'),(23,'799fc6a053efecb3b07f6cdd37dd6e62');
/*!40000 ALTER TABLE `CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CONFIG_PART`
--

DROP TABLE IF EXISTS `CONFIG_PART`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `CONFIG_PART` (
  `CONFIG_ID` int(11) NOT NULL,
  `PART_ID` int(11) NOT NULL,
  `PART_QTY` int(3) NOT NULL,
  PRIMARY KEY (`CONFIG_ID`,`PART_ID`),
  KEY `fk_Config_Parts_Configuration1_idx` (`CONFIG_ID`),
  KEY `fk_Config_Parts_Part1_idx` (`PART_ID`),
  CONSTRAINT `fk_Config_Parts_Configuration1` FOREIGN KEY (`CONFIG_ID`) REFERENCES `CONFIG` (`config_id`),
  CONSTRAINT `fk_Config_Parts_Part1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CONFIG_PART`
--

LOCK TABLES `CONFIG_PART` WRITE;
/*!40000 ALTER TABLE `CONFIG_PART` DISABLE KEYS */;
INSERT INTO `CONFIG_PART` VALUES (1,2,1),(1,5,1),(1,10,1),(1,16,1),(1,19,1),(1,21,1),(1,26,2),(2,3,1),(2,5,1),(2,7,1),(2,15,1),(2,19,1),(2,21,1),(2,25,2),(3,1,1),(3,5,1),(3,8,1),(3,15,1),(3,19,1),(3,21,1),(3,26,2),(4,2,1),(4,5,1),(4,9,1),(4,15,1),(4,18,1),(4,23,1),(4,25,2),(5,3,1),(5,5,1),(5,9,1),(5,14,1),(5,18,1),(5,23,1),(5,25,2),(6,2,1),(6,5,1),(6,11,1),(6,16,1),(6,18,1),(6,21,1),(6,25,2),(7,1,1),(7,5,1),(7,11,1),(7,14,1),(7,19,1),(7,23,1),(7,26,2),(8,3,1),(8,5,1),(8,7,1),(8,16,1),(8,18,1),(8,23,1),(8,25,2),(9,2,1),(9,5,1),(9,10,1),(9,15,1),(9,19,1),(9,21,1),(9,25,2),(10,2,1),(10,5,1),(10,8,1),(10,13,1),(10,19,1),(10,22,1),(10,26,2),(11,2,1),(11,5,1),(11,7,1),(11,13,1),(11,18,1),(11,21,1),(11,26,2),(12,1,1),(12,5,1),(12,7,1),(12,15,1),(12,18,1),(12,23,1),(12,25,2),(13,2,1),(13,5,1),(13,10,1),(13,16,1),(13,18,1),(13,22,1),(13,26,2),(14,1,1),(14,5,1),(14,11,1),(14,16,1),(14,18,1),(14,23,1),(14,26,2),(15,2,1),(15,5,1),(15,8,1),(15,13,1),(15,18,1),(15,22,1),(15,26,2),(16,3,1),(16,5,1),(16,9,1),(16,15,1),(16,19,1),(16,23,1),(16,26,2),(17,2,1),(17,5,1),(17,8,1),(17,16,1),(17,19,1),(17,22,1),(17,25,2),(18,2,1),(18,5,1),(18,11,1),(18,14,1),(18,18,1),(18,21,1),(18,25,2),(19,2,1),(19,5,1),(19,11,1),(19,14,1),(19,19,1),(19,22,1),(19,26,2),(20,2,1),(20,5,1),(20,9,1),(20,15,1),(20,18,1),(20,22,1),(20,26,2),(21,3,1),(21,5,1),(21,11,1),(21,14,1),(21,19,1),(21,23,1),(21,25,2),(22,1,1),(22,5,1),(22,8,1),(22,15,1),(22,18,1),(22,23,1),(22,26,2),(23,3,1),(23,5,1),(23,7,1),(23,14,1),(23,18,1),(23,23,1),(23,26,2);
/*!40000 ALTER TABLE `CONFIG_PART` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CONTACT`
--

DROP TABLE IF EXISTS `CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `CONTACT` (
  `CONTACT_ID` int(11) NOT NULL,
  `CONTACT_STREET` varchar(45) NOT NULL,
  `CONTACT_PHONE` varchar(45) NOT NULL,
  `ZIPCODE` char(5) NOT NULL,
  PRIMARY KEY (`CONTACT_ID`),
  KEY `fk_CONTACT_ZIPCODE1_idx` (`ZIPCODE`),
  CONSTRAINT `fk_CONTACT_ZIPCODE1` FOREIGN KEY (`ZIPCODE`) REFERENCES `ZIP` (`zipcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CONTACT`
--

LOCK TABLES `CONTACT` WRITE;
/*!40000 ALTER TABLE `CONTACT` DISABLE KEYS */;
INSERT INTO `CONTACT` VALUES (1,'401 2nd Ave S','206-555-7542','98104'),(2,'16541 5th ave NE','206-555-4846','98155'),(3,'15482 Pasadena Ave','714-321-5555','92780'),(4,'15884 Rose Ave','714-544-5566','95030'),(5,'20625 S Reeder Ln','503-655-1166','97045');
/*!40000 ALTER TABLE `CONTACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COOLER`
--

DROP TABLE IF EXISTS `COOLER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `COOLER` (
  `PART_ID` int(11) NOT NULL,
  `RAD_SIZE_MM` int(4) NOT NULL,
  PRIMARY KEY (`PART_ID`),
  KEY `fk_COOLER_PART1_idx` (`PART_ID`),
  CONSTRAINT `fk_COOLER_PART1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COOLER`
--

LOCK TABLES `COOLER` WRITE;
/*!40000 ALTER TABLE `COOLER` DISABLE KEYS */;
INSERT INTO `COOLER` VALUES (5,240),(6,120);
/*!40000 ALTER TABLE `COOLER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CPU`
--

DROP TABLE IF EXISTS `CPU`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `CPU` (
  `PART_ID` int(11) NOT NULL,
  `SOCKET_ID` int(11) NOT NULL,
  `CPU_CLOCKSPEED_MHZ` int(11) DEFAULT NULL,
  PRIMARY KEY (`PART_ID`),
  KEY `fk_CPU_PART1_idx` (`PART_ID`),
  KEY `fk_CPU_SOCKET1_idx` (`SOCKET_ID`),
  CONSTRAINT `fk_CPU_PART1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`),
  CONSTRAINT `fk_CPU_SOCKET1` FOREIGN KEY (`SOCKET_ID`) REFERENCES `SOCKET` (`socket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CPU`
--

LOCK TABLES `CPU` WRITE;
/*!40000 ALTER TABLE `CPU` DISABLE KEYS */;
INSERT INTO `CPU` VALUES (7,1,5000),(8,1,5000),(9,1,4800),(10,1,4800),(11,1,4000),(12,2,4400);
/*!40000 ALTER TABLE `CPU` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CUSTOMER`
--

DROP TABLE IF EXISTS `CUSTOMER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `CUSTOMER` (
  `CUSTOMER_ID` int(11) NOT NULL,
  `CUSTOMER_FNAME` varchar(45) DEFAULT NULL,
  `CUSTOMER_LNAME` varchar(45) DEFAULT NULL,
  `CONTACT_ID` int(11) NOT NULL,
  `PAYMENT_METHOD_ID` int(11) NOT NULL,
  PRIMARY KEY (`CUSTOMER_ID`),
  UNIQUE KEY `idCustomer_UNIQUE` (`CUSTOMER_ID`),
  KEY `fk_CUSTOMER_CONTACT1_idx` (`CONTACT_ID`),
  KEY `fk_CUSTOMER_PAYMENT_METHOD1_idx` (`PAYMENT_METHOD_ID`),
  CONSTRAINT `fk_CUSTOMER_CONTACT1` FOREIGN KEY (`CONTACT_ID`) REFERENCES `CONTACT` (`contact_id`),
  CONSTRAINT `fk_CUSTOMER_PAYMENT_METHOD1` FOREIGN KEY (`PAYMENT_METHOD_ID`) REFERENCES `PAYMENT_METHOD` (`payment_method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CUSTOMER`
--

LOCK TABLES `CUSTOMER` WRITE;
/*!40000 ALTER TABLE `CUSTOMER` DISABLE KEYS */;
INSERT INTO `CUSTOMER` VALUES (1,'Bob','Doe',1,1),(2,'John','Smith',2,1),(3,'Brandon','Zimmerman',3,2),(4,'Alan','Schott',4,1),(5,'April','Hunter',5,2);
/*!40000 ALTER TABLE `CUSTOMER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FORM_FACTOR`
--

DROP TABLE IF EXISTS `FORM_FACTOR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `FORM_FACTOR` (
  `FORM_FACTOR_ID` int(11) NOT NULL,
  `FORM_FACTOR_TYPE` varchar(45) NOT NULL,
  PRIMARY KEY (`FORM_FACTOR_ID`),
  UNIQUE KEY `FORM_FACTOR_TYPE_UNIQUE` (`FORM_FACTOR_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FORM_FACTOR`
--

LOCK TABLES `FORM_FACTOR` WRITE;
/*!40000 ALTER TABLE `FORM_FACTOR` DISABLE KEYS */;
INSERT INTO `FORM_FACTOR` VALUES (2,'ATX'),(1,'ITX');
/*!40000 ALTER TABLE `FORM_FACTOR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GPU`
--

DROP TABLE IF EXISTS `GPU`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `GPU` (
  `PART_ID` int(11) NOT NULL,
  `GPU_CLOCK_SPEED_MHZ` int(11) NOT NULL,
  `GPU_MIN_POWER_WATTS` int(5) NOT NULL,
  PRIMARY KEY (`PART_ID`),
  KEY `fk_GPU_PART1_idx` (`PART_ID`),
  CONSTRAINT `fk_GPU_PART1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GPU`
--

LOCK TABLES `GPU` WRITE;
/*!40000 ALTER TABLE `GPU` DISABLE KEYS */;
INSERT INTO `GPU` VALUES (13,1600,550),(14,1600,650),(15,1600,650),(16,1500,400),(17,1300,500);
/*!40000 ALTER TABLE `GPU` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `INVENTORY`
--

DROP TABLE IF EXISTS `INVENTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `INVENTORY` (
  `PART_ID` int(11) NOT NULL,
  `LOCATION_ID` int(11) NOT NULL,
  `INVENTORY_QTY` int(11) NOT NULL,
  PRIMARY KEY (`PART_ID`,`LOCATION_ID`),
  UNIQUE KEY `Part_PART_ID_UNIQUE` (`PART_ID`),
  KEY `fk_INVENTORY_Part1_idx` (`PART_ID`),
  KEY `fk_INVENTORY_LOCATION1_idx` (`LOCATION_ID`),
  CONSTRAINT `fk_INVENTORY_LOCATION1` FOREIGN KEY (`LOCATION_ID`) REFERENCES `LOCATION` (`location_id`),
  CONSTRAINT `fk_INVENTORY_Part1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `INVENTORY`
--

LOCK TABLES `INVENTORY` WRITE;
/*!40000 ALTER TABLE `INVENTORY` DISABLE KEYS */;
INSERT INTO `INVENTORY` VALUES (2,1,2),(5,2,3),(9,3,5),(11,4,2),(14,5,1),(16,6,3),(19,7,2),(23,1,3),(24,2,3),(26,3,3),(27,4,1),(29,5,3),(31,6,3);
/*!40000 ALTER TABLE `INVENTORY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LOCATION`
--

DROP TABLE IF EXISTS `LOCATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `LOCATION` (
  `LOCATION_ID` int(11) NOT NULL,
  `AISLE_NUM` int(11) NOT NULL,
  `BIN_NUM` int(11) NOT NULL,
  PRIMARY KEY (`LOCATION_ID`),
  UNIQUE KEY `LOCATION_ID_UNIQUE` (`LOCATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LOCATION`
--

LOCK TABLES `LOCATION` WRITE;
/*!40000 ALTER TABLE `LOCATION` DISABLE KEYS */;
INSERT INTO `LOCATION` VALUES (1,1,1),(2,1,2),(3,2,1),(4,2,2),(5,3,1),(6,3,2),(7,3,3);
/*!40000 ALTER TABLE `LOCATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MOTHERBOARD`
--

DROP TABLE IF EXISTS `MOTHERBOARD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `MOTHERBOARD` (
  `PART_ID` int(11) NOT NULL,
  `FORM_FACTOR_ID` int(11) NOT NULL,
  `MOTHERBOARD_NUM_RAM_SLOTS` int(11) NOT NULL,
  `SOCKET_ID` int(11) NOT NULL,
  PRIMARY KEY (`PART_ID`),
  KEY `fk_MOTHERBOARD_FORM_FACTOR1_idx` (`FORM_FACTOR_ID`),
  KEY `fk_MOTHERBOARD_SOCKET1_idx` (`SOCKET_ID`),
  CONSTRAINT `fk_MOTHERBOARD_FORM_FACTOR1` FOREIGN KEY (`FORM_FACTOR_ID`) REFERENCES `FORM_FACTOR` (`form_factor_id`),
  CONSTRAINT `fk_MOTHERBOARD_PART1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`),
  CONSTRAINT `fk_MOTHERBOARD_SOCKET1` FOREIGN KEY (`SOCKET_ID`) REFERENCES `SOCKET` (`socket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MOTHERBOARD`
--

LOCK TABLES `MOTHERBOARD` WRITE;
/*!40000 ALTER TABLE `MOTHERBOARD` DISABLE KEYS */;
INSERT INTO `MOTHERBOARD` VALUES (18,2,4,1),(19,2,4,1),(20,1,2,2);
/*!40000 ALTER TABLE `MOTHERBOARD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ORDER`
--

DROP TABLE IF EXISTS `ORDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ORDER` (
  `ORDER_ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `ORDER_STATUS_ID` int(2) NOT NULL,
  PRIMARY KEY (`ORDER_ID`,`CUSTOMER_ID`,`ORDER_STATUS_ID`),
  UNIQUE KEY `idOrder_UNIQUE` (`ORDER_ID`),
  KEY `fk_ORDER_CUSTOMER1_idx` (`CUSTOMER_ID`),
  KEY `fk_ORDER_ORDER_STATUS1_idx` (`ORDER_STATUS_ID`),
  CONSTRAINT `fk_ORDER_CUSTOMER1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `CUSTOMER` (`customer_id`),
  CONSTRAINT `fk_ORDER_ORDER_STATUS1` FOREIGN KEY (`ORDER_STATUS_ID`) REFERENCES `ORDER_STATUS` (`order_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ORDER_STATUS_ORDER_STATUS';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ORDER`
--

LOCK TABLES `ORDER` WRITE;
/*!40000 ALTER TABLE `ORDER` DISABLE KEYS */;
INSERT INTO `ORDER` VALUES (1,1,2),(2,1,2),(3,2,2),(4,3,3),(5,4,2),(6,3,1),(7,5,1);
/*!40000 ALTER TABLE `ORDER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ORDER_CONFIG`
--

DROP TABLE IF EXISTS `ORDER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ORDER_CONFIG` (
  `ORDER_ID` int(11) NOT NULL,
  `CONFIG_ID` int(11) NOT NULL,
  `CONFIG_QTY` varchar(45) NOT NULL,
  PRIMARY KEY (`ORDER_ID`,`CONFIG_ID`),
  KEY `fk_ORDER_has_CONFIG_CONFIG1_idx` (`CONFIG_ID`),
  KEY `fk_ORDER_has_CONFIG_ORDER1_idx` (`ORDER_ID`),
  CONSTRAINT `fk_ORDER_has_CONFIG_CONFIG1` FOREIGN KEY (`CONFIG_ID`) REFERENCES `CONFIG` (`config_id`),
  CONSTRAINT `fk_ORDER_has_CONFIG_ORDER1` FOREIGN KEY (`ORDER_ID`) REFERENCES `ORDER` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ORDER_CONFIG`
--

LOCK TABLES `ORDER_CONFIG` WRITE;
/*!40000 ALTER TABLE `ORDER_CONFIG` DISABLE KEYS */;
INSERT INTO `ORDER_CONFIG` VALUES (1,2,'2'),(1,3,'4'),(1,10,'4'),(1,13,'4'),(2,7,'1'),(2,8,'3'),(2,11,'2'),(3,4,'1'),(3,8,'5'),(4,4,'5'),(4,16,'2'),(4,17,'4'),(4,22,'4'),(5,2,'1'),(5,4,'3'),(5,6,'5'),(5,12,'4'),(5,15,'4'),(6,20,'5'),(7,8,'2'),(7,11,'4'),(7,22,'3');
/*!40000 ALTER TABLE `ORDER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ORDER_STATUS`
--

DROP TABLE IF EXISTS `ORDER_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ORDER_STATUS` (
  `ORDER_STATUS_ID` int(2) NOT NULL,
  `ORDER_STATUS_CODE` varchar(25) NOT NULL,
  PRIMARY KEY (`ORDER_STATUS_ID`),
  UNIQUE KEY `ORDER_STATUS_CODE_UNIQUE` (`ORDER_STATUS_CODE`),
  UNIQUE KEY `ORDER_STATUS_ID_UNIQUE` (`ORDER_STATUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ORDER_STATUS`
--

LOCK TABLES `ORDER_STATUS` WRITE;
/*!40000 ALTER TABLE `ORDER_STATUS` DISABLE KEYS */;
INSERT INTO `ORDER_STATUS` VALUES (3,'CANCELLED'),(2,'COMPLETE'),(1,'PENDING');
/*!40000 ALTER TABLE `ORDER_STATUS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PART`
--

DROP TABLE IF EXISTS `PART`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `PART` (
  `PART_ID` int(11) NOT NULL,
  `VENDOR_ID` int(11) NOT NULL,
  `PART_NAME` varchar(45) NOT NULL,
  `DESCRIPTION` varchar(95) DEFAULT '',
  `SERIAL_NUMBER` varchar(45) NOT NULL,
  `UNIT_PRICE_USD` int(11) NOT NULL,
  `PART_TYPE` char(3) NOT NULL,
  PRIMARY KEY (`PART_ID`),
  UNIQUE KEY `idInventory_UNIQUE` (`PART_ID`),
  UNIQUE KEY `SERIAL_NUMBER_UNIQUE` (`SERIAL_NUMBER`),
  KEY `fk_Part_VENDOR1_idx` (`VENDOR_ID`),
  KEY `fk_PART_PART_TYPE_LOOKUP1_idx` (`PART_TYPE`),
  CONSTRAINT `fk_PART_PART_TYPE_LOOKUP1` FOREIGN KEY (`PART_TYPE`) REFERENCES `PART_TYPE_LOOKUP` (`part_type`),
  CONSTRAINT `fk_Part_VENDOR1` FOREIGN KEY (`VENDOR_ID`) REFERENCES `VENDOR` (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PART`
--

LOCK TABLES `PART` WRITE;
/*!40000 ALTER TABLE `PART` DISABLE KEYS */;
INSERT INTO `PART` VALUES (1,1,'Lian-Li PC-O5SX','Lian Li ITX Case','PC-O5SX',290,'CAS'),(2,2,'PHANTEKS ENTHOO EVOLV','PHANTEKS ENTHOO EVOLV ATX Case','PH-ES515ETG_BK',170,'CAS'),(3,3,'NZXT H700i','NZXT H700i ATX Case','CA-H700W-WB',190,'CAS'),(4,4,'THERMALTAKE VIEW 31 RGB','THERMALTAKE VIEW 31 RGB ATX Case','CA-1H8-00M1WN-01',110,'CAS'),(5,5,'ASUS ROG Ryuo 240 RGB','Asus 240mm Liquid AIO CPU Cooler','90RC0040-M0UAY0',170,'COL'),(6,6,'Cooler Master MasterLiquid ML120R','Coolermaster 120mm Liquid AIO CPU Cooler','MLX-D12M-A20PC-R1',100,'COL'),(7,7,'Intel i9-9900K','Intel i9-9900K 8 Core CPU','BX80684I99900K',580,'CPU'),(8,7,'Intel i7-9700K','Intel i7-9700K 6 Core CPU','BX80684I79700K',420,'CPU'),(9,7,'Intel i7-8700K','Intel i7-8700K 6 Core CPU','BX80684I78700K',390,'CPU'),(10,7,'Intel i5-8600K','Intel i5-8600K 6 Core CPU','BX80684I58600K',270,'CPU'),(11,7,'Intel i5-8400','Intel i5-8400 6 Core CPU','BX80684I58400',240,'CPU'),(12,8,'AMD 2700X','AMD 2700X 12 Core CPU','YD270XBGAFBOX',310,'CPU'),(13,9,'EVGA GTX 1080 FTW','EVGA GTX 1080 FTW','08G-P4-6284-KR',530,'GPU'),(14,10,'MSI RTX 2080 Ti GAMING X','MSI RTX 2080 Ti GAMING X','RTX2080TIDUKE11GOC',1390,'GPU'),(15,9,'EVGA RTX 2080 XC GAMING','EVGA RTX 2080 XC GAMING','08G-P4-2182-KR',800,'GPU'),(16,11,'GIGABYTE GEFORCE GTX 1060 ','GIGABYTE GEFORCE GTX 1060 ','GV-N1060WF2OC-3GD',190,'GPU'),(17,12,'ZOTAC GTX 1070 MINI','ZOTAC GTX 1070 MINI','ZT-P10700G-10M',390,'GPU'),(18,13,'X470 Taichi Ultimate','X470TaichiUltimate ATX Motherboard AMD','X470TaichiUltimate',270,'MOB'),(19,13,'Z370 Extreme 4','Z370Extreme4 ATX Motherboard INTEL','Z370Extreme4',165,'MOB'),(20,5,'StrixZ370-I Gaming','StrixZ370-I Gaming ITX Motherboard INTEL','StrixZ370-IGaming',180,'MOB'),(21,9,'EVGA 500 B1','EVGA 500W Bronze','100-B1-0500-KR',40,'PSU'),(22,9,'EVGA 600 B1','EVGA 600W Bronze','100-B1-0600-KR',60,'PSU'),(23,9,'EVGA SUPERNOVA 650GM','EVGA SUPERNOVA 650GM','123-GM-0650-Y1',130,'PSU'),(24,9,'EVGA SUPERNOVA 1200 P2','EVGA SUPERNOVA 1200 P2','220-P2-1200-X1',180,'PSU'),(25,14,'BALLISTIX 8GB Sport LT','BALLISTIX 8GB Sport LT','BLS8G4D240FSE',70,'RAM'),(26,15,'CORSAIR DOMINATOR PLAT 16GB','CORSAIR DOMINATOR PLAT 16GB','CMD16GX4M2B3200C16',270,'RAM'),(27,16,'G.SKILL TridentZ RGB Series 32GB','G.SKILL TridentZ RGB Series 32GB','F4-3200C16Q-32GTZRX',340,'RAM'),(28,7,'INTEL 545S','INTEL 545S SSD','SSDSC2KW256G8X1',50,'STO'),(29,17,'SAMSUNG 970 EVO','SAMSUNG 970 EVO SSD','MZ-V7E500BW',150,'STO'),(30,17,'SAMSUNG 860 EVO','SAMSUNG 860 EVO SSD','MZ-76E1T0B/AM',160,'STO'),(31,18,'SEAGATE 2TB BarraCuda','SEAGATE 2TB BarraCuda HDD','ST2000DM006',60,'STO');
/*!40000 ALTER TABLE `PART` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PART_TYPE_LOOKUP`
--

DROP TABLE IF EXISTS `PART_TYPE_LOOKUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `PART_TYPE_LOOKUP` (
  `PART_TYPE` char(3) NOT NULL,
  `PART_TYPE_TABLENAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PART_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PART_TYPE_LOOKUP`
--

LOCK TABLES `PART_TYPE_LOOKUP` WRITE;
/*!40000 ALTER TABLE `PART_TYPE_LOOKUP` DISABLE KEYS */;
INSERT INTO `PART_TYPE_LOOKUP` VALUES ('CAS','CASE'),('COL','COOLER'),('CPU','CPU'),('GPU','GPU'),('MOB','MOTHERBOARD'),('PSU','PSU'),('RAM','RAM'),('STO','STORAGE');
/*!40000 ALTER TABLE `PART_TYPE_LOOKUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PAYMENT_METHOD`
--

DROP TABLE IF EXISTS `PAYMENT_METHOD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `PAYMENT_METHOD` (
  `PAYMENT_METHOD_ID` int(11) NOT NULL,
  `PAYMENT_METHOD_NAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PAYMENT_METHOD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PAYMENT_METHOD`
--

LOCK TABLES `PAYMENT_METHOD` WRITE;
/*!40000 ALTER TABLE `PAYMENT_METHOD` DISABLE KEYS */;
INSERT INTO `PAYMENT_METHOD` VALUES (1,'CARD'),(2,'EFT');
/*!40000 ALTER TABLE `PAYMENT_METHOD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PSU`
--

DROP TABLE IF EXISTS `PSU`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `PSU` (
  `PART_ID` int(11) NOT NULL,
  `PSU_POWER_RATING` int(5) NOT NULL,
  PRIMARY KEY (`PART_ID`),
  KEY `fk_PSU_PART1_idx` (`PART_ID`),
  CONSTRAINT `fk_PSU_PART1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PSU`
--

LOCK TABLES `PSU` WRITE;
/*!40000 ALTER TABLE `PSU` DISABLE KEYS */;
INSERT INTO `PSU` VALUES (21,500),(22,600),(23,650),(24,1200);
/*!40000 ALTER TABLE `PSU` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RAM`
--

DROP TABLE IF EXISTS `RAM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `RAM` (
  `PART_ID` int(11) NOT NULL,
  `RAM_CAPACITY_GB` int(4) NOT NULL,
  `RAM_SPEED_MHZ` int(11) NOT NULL,
  PRIMARY KEY (`PART_ID`),
  CONSTRAINT `fk_RAM_PART1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RAM`
--

LOCK TABLES `RAM` WRITE;
/*!40000 ALTER TABLE `RAM` DISABLE KEYS */;
INSERT INTO `RAM` VALUES (25,8,3000),(26,16,3000),(27,32,3200);
/*!40000 ALTER TABLE `RAM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SOCKET`
--

DROP TABLE IF EXISTS `SOCKET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `SOCKET` (
  `SOCKET_ID` int(11) NOT NULL,
  `SOCKET_TYPE` varchar(45) NOT NULL,
  PRIMARY KEY (`SOCKET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SOCKET`
--

LOCK TABLES `SOCKET` WRITE;
/*!40000 ALTER TABLE `SOCKET` DISABLE KEYS */;
INSERT INTO `SOCKET` VALUES (1,'LGA 1151'),(2,'AM4');
/*!40000 ALTER TABLE `SOCKET` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STORAGE`
--

DROP TABLE IF EXISTS `STORAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `STORAGE` (
  `PART_ID` int(11) NOT NULL,
  `STORAGE_SIZE_GB` int(11) NOT NULL,
  `STORAGE_TYPE_ID` int(11) NOT NULL,
  PRIMARY KEY (`PART_ID`),
  KEY `fk_STORAGE_PART1_idx` (`PART_ID`),
  KEY `fk_STORAGE_STORAGE_TYPE1_idx` (`STORAGE_TYPE_ID`),
  CONSTRAINT `fk_STORAGE_PART1` FOREIGN KEY (`PART_ID`) REFERENCES `PART` (`part_id`),
  CONSTRAINT `fk_STORAGE_STORAGE_TYPE1` FOREIGN KEY (`STORAGE_TYPE_ID`) REFERENCES `STORAGE_TYPE` (`storage_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORAGE`
--

LOCK TABLES `STORAGE` WRITE;
/*!40000 ALTER TABLE `STORAGE` DISABLE KEYS */;
INSERT INTO `STORAGE` VALUES (28,256,1),(29,500,1),(30,1000,1),(31,2000,2);
/*!40000 ALTER TABLE `STORAGE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STORAGE_TYPE`
--

DROP TABLE IF EXISTS `STORAGE_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `STORAGE_TYPE` (
  `STORAGE_TYPE_ID` int(11) NOT NULL,
  `STORAGE_TYPE_NAME` varchar(45) NOT NULL,
  PRIMARY KEY (`STORAGE_TYPE_ID`),
  UNIQUE KEY `STORAGE_TYPE_UNIQUE` (`STORAGE_TYPE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORAGE_TYPE`
--

LOCK TABLES `STORAGE_TYPE` WRITE;
/*!40000 ALTER TABLE `STORAGE_TYPE` DISABLE KEYS */;
INSERT INTO `STORAGE_TYPE` VALUES (2,'HDD'),(1,'SSD');
/*!40000 ALTER TABLE `STORAGE_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VENDOR`
--

DROP TABLE IF EXISTS `VENDOR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `VENDOR` (
  `VENDOR_ID` int(11) NOT NULL,
  `VENDOR_NAME` varchar(45) NOT NULL,
  PRIMARY KEY (`VENDOR_ID`),
  UNIQUE KEY `idVendor_UNIQUE` (`VENDOR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VENDOR`
--

LOCK TABLES `VENDOR` WRITE;
/*!40000 ALTER TABLE `VENDOR` DISABLE KEYS */;
INSERT INTO `VENDOR` VALUES (1,'LIANLI'),(2,'PHANTEKS'),(3,'NZXT'),(4,'THERMALTAKE'),(5,'ASUS'),(6,'COOLERMASTER'),(7,'INTEL'),(8,'AMD'),(9,'EVGA'),(10,'MSI'),(11,'GIGABYTE'),(12,'ZOTAC'),(13,'ASROCK'),(14,'BALLISTIX'),(15,'CORSAIR'),(16,'G.SKILL'),(17,'SAMSUNG'),(18,'SEAGATE');
/*!40000 ALTER TABLE `VENDOR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ZIP`
--

DROP TABLE IF EXISTS `ZIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ZIP` (
  `ZIPCODE` char(5) NOT NULL,
  `CITY` varchar(45) DEFAULT NULL,
  `STATE` char(2) DEFAULT NULL,
  PRIMARY KEY (`ZIPCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ZIP`
--

LOCK TABLES `ZIP` WRITE;
/*!40000 ALTER TABLE `ZIP` DISABLE KEYS */;
INSERT INTO `ZIP` VALUES ('92780','Tustin','CA'),('95030','Los Gatos','CA'),('97045','Oregon City','OR'),('98104','Seattle','WA'),('98155','Shoreline','WA');
/*!40000 ALTER TABLE `ZIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'CUSTOMPC'
--
/*!50003 DROP PROCEDURE IF EXISTS `update_PartIn_Inventory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_PartIn_Inventory`(
	in partID	int,
    in quantity int 
)
BEGIN


UPDATE CUSTOMPC.INVENTORY
SET 
	INVENTORY_QTY = quantity
WHERE PART_ID = partID;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-05 22:56:58
